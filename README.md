# DocusaurusTemplate

Ce dépot sert de modèle de départ pour réaliser sa documentation en ligne.

Ce site est utilise [Docusaurus](https://docusaurus.io/).

## Procédure

### Copie du dépot

Faites un fork du dépot.

### Modifiaction du fichier de configuration

Modifiez le fichier de configuration ``docusaurus.config.json`` :
```
// ...  
url: 'https://nom_utilisateur.gitlab.io',   
baseUrl: '/nom_projet/',  
projectName: 'nom_projet',  
organizationName: 'nom_utilisateur',  
// ...  
```
Actuellement ``nom_utilisateur`` et ``nom_projet`` sont ``juliengoetghebeur`` et ``docusaurustemplate``. Remplacez les par les infos de votre dépot.

### Personnalisation

La page d'accueil : ``./src/pages/index.js``

Les pages de documentation sont à ajouter dans ``./docs``.


Pour plus d'information visitez [la doc officielle docusaurus](https://docusaurus.io/docs)
